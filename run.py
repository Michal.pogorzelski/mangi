from flask import Flask, render_template, request, url_for, redirect

app=Flask(__name__)

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/index', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        return redirect(url_for('index'))

    return render_template('index.html')

@app.route('/list', methods=['GET', 'POST'])
def list():
    if request.method == 'POST':
        return redirect(url_for('list'))

    return render_template('list.html')

@app.route('/new', methods=['GET', 'POST'])
def new():
    if request.method == 'POST':
        return redirect(url_for('new'))

    return render_template('new.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return redirect(url_for('login'))

    return render_template('login.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        return redirect(url_for('register'))

    return render_template('register.html')

if __name__=='__main__':
    app.run(debug=True)
